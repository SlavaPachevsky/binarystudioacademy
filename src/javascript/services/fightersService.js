import { callApi } from '../helpers/apiHelper';
import { fighters } from '../helpers/mockData';

class FighterService {
  async getFighterDetails(id) {
    // todo: implement this method // endpoint - `details/fighter/${id}.json`;
    //ok
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');      
      return apiResult;
    } catch (error) {
      throw error;
    }
  }
  async getFighterInfo(id){
    const fighterDetails = await this.getFighterDetails(id);
    return fighterDetails;
  } 
  
}

export const fighterService = new FighterService();
