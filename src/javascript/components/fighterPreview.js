import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
    attributes: {style: 'color: white; font-size:1.2rem'}
  });
  // todo: show fighter info (image, name, health, etc.) 
  //ok
  if (fighter) {
    const { name, health, attack, defense} = fighter;
    fighterElement.append(fighterPreview(name, health, attack, defense));
  }
  return fighterElement;
}
function fighterPreview(name, health, attack, defense) {
  const div = createElement({
    tagName:'div'
  })
  div.innerText = `name: ${name}
                    health: ${health}
                    attack: ${attack}
                    defense: ${defense}`;
  return div;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
