import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let isFightEnd = false;
    let pressed = new Set();
    let  beginHealth = [firstFighter.health, secondFighter.health];
    let times = { timePrevSecond:0, timePrevFirst:0};

    document.addEventListener('keydown', function (e) {
      if (e.repeat) return;
      if ( pressed.has(controls.PlayerOneAttack) && e.code == controls.PlayerOneBlock) return;
      if ( pressed.has(controls.PlayerOneBlock) && e.code == controls.PlayerOneAttack) return;
      if ( pressed.has(controls.PlayerTwoAttack) && e.code == controls.PlayerTwoBlock) return;
      if ( pressed.has(controls.PlayerTwoBlock) && e.code == controls.PlayerTwoAttack) return;
      pressed.add(e.code);

      isFightEnd = stepFight(firstFighter, secondFighter, pressed, times);
      changeIndicateHealth(firstFighter, secondFighter, beginHealth);  
      if (isFightEnd) {
        const winner = (firstFighter.health > secondFighter.health) ? firstFighter : 
                (firstFighter.health == secondFighter.health) ? {name: 'No winner'} : secondFighter;
        resolve(winner)
      }
    })

    document.addEventListener('keyup', function(e) {
      pressed.delete(e.code);
    })  
  })

  function isAllCodes (pressed, ...codes) {
    for (let code of codes) {
        if (!pressed.has(code) ) return false;
    }
    return true
  }
  function changeIndicateHealth(firstFighter, secondFighter, beginHealth) {
    let width;
    if (firstFighter && (beginHealth[0] != firstFighter.health) ) {
       width = 100 - (beginHealth[0] - firstFighter.health)/beginHealth[0] * 100;
      document.getElementById('left-fighter-indicator').style.width =  Math.floor(width)+'%';
     }
    if (secondFighter && (beginHealth[1] != secondFighter.health)) {
      width = 100 - (beginHealth[1] - secondFighter.health)/beginHealth[1] * 100 ;
      document.getElementById('right-fighter-indicator').style.width = Math.floor(width)+'%';
     }
  }
  
  function stepFight(firstFighter, secondFighter, setCodes, times) {
    let timeCur;
      switch (true) {
        case isAllCodes(setCodes, controls.PlayerOneAttack, controls.PlayerTwoAttack):
          secondFighter.health -= getDamage(firstFighter);
          firstFighter.health -= getDamage(secondFighter);
          //console.log('AJ')
          break;
        case isAllCodes(setCodes, controls.PlayerOneAttack, controls.PlayerTwoBlock):
          secondFighter.health -= getDamage(firstFighter, secondFighter);
          //console.log('AL')
          break;
        case isAllCodes(setCodes, controls.PlayerTwoAttack, controls.PlayerOneBlock):
          firstFighter.health -= getDamage(secondFighter, firstFighter);
          //console.log('JD')
          break;
        case isAllCodes(setCodes, ...controls.PlayerOneCriticalHitCombination):
          timeCur = performance.now()
          if ( ( timeCur - times.timePrevFirst) > 10000) secondFighter.health -= firstFighter.attack*2;
          times.timePrevFirst = timeCur;
          //console.log('QWE')
          break;
        case isAllCodes(setCodes, ...controls.PlayerTwoCriticalHitCombination):
          timeCur = performance.now()           
          if ( ( timeCur - times.timePrevSecond ) > 10000) firstFighter.health -= secondFighter.attack*2;
          times.timePrevSecond = timeCur;
          //console.log('UIO')
          break;
        case isAllCodes(setCodes, controls.PlayerOneAttack) :
          secondFighter.health -= getDamage(firstFighter);
          //console.log('A')
          break;
        case isAllCodes(setCodes, controls. PlayerTwoAttack) :
          firstFighter.health -= getDamage(secondFighter);
          //console.log('J')
          break;
        default:
        break;
      }
    return  ( (firstFighter.health <= 0) ||  (secondFighter.health <= 0) || (setCodes.has("Escape")) );      
  }
}

export function getDamage(attacker, defender) {
  // return damage
  //ok
  const damage = getHitPower(attacker) - getBlockPower(defender)
  return (damage>0) ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  //ok
  const criticalHitChance = Math.random(1)+1
  const power = fighter.attack * criticalHitChance;
  return power
}

export function getBlockPower(fighter) {
  // return block power
  //ok
  if (fighter) {
    const dodgeChance = Math.random(1)+1;
    const power = fighter.defense * dodgeChance;
    return power
  }
  return 0
}
